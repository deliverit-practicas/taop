#!/bin/bash

checking=`cmp $1 $2`
result=$?

if [ $result -eq 0 ]; then
	echo "Accepted Answer"
else
	echo "$checking"
	line=`echo "$checking" | cut -d ':' -f 2 | cut -d ' ' -f 5`
	diff -y $1 $2 | nl | grep -A 5 -B 5 '^ *'"$line"'\s'
fi
