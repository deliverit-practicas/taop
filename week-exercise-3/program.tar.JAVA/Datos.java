
import java.io.*;
import java.util.*;

class Datos {
	static void run() {
		try (Scanner sc = new Scanner(new BufferedInputStream(System.in))) {
			String line = sc.nextLine();
			System.out.println(line);
		} catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}
	}
}
