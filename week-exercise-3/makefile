
########### General Settings #################

TARGET_EXEC := program.run
OUTPUT := student_output.txt
EXPECTED_OUTPUT := correct_output.txt
INPUT := input.txt

TIME_OUT := 5
TIME_OUT_CMD := timeout -s KILL $(TIME_OUT)
MEASURE_TIME := /bin/time

# TIMING_CMD := $(TIME_OUT_CMD) $(MEASURE_TIME)
TIMING_CMD := $(TIME_OUT_CMD) 

BUILD_DIR := ./build

SHELL := bash

## Moving Sources from program.tar ##########

# SOURCE_DIR := program.tar.JAVA
SOURCE_DIR := program.tar

SOURCES := $(wildcard $(SOURCE_DIR)/*)

listing.txt: $(SOURCES)
	cp -vf $(SOURCE_DIR)/* . | tee listing.txt

######## Cleaning (For debugging) ##########

.PHONY: clean
clean:
	rm -f *.c *.java *.py *.cpp *.class listing.txt $(OUTPUT)
	rm -Rf $(BUILD_DIR) 

######## Build exercise environment ##########

Entorno.tar:
	tar -cvf Entorno.tar makefile checking.sh input.txt correct_output.txt 

########### C compilation #################

CC := gcc
CFLAGS := -O3 -Wall -Wextra # -g

# All .c files
SRCS := $(wildcard *.c)

# Conditional C build rules
ifneq ("$(SRCS)","")

# Will compile into .o, for example: hello.c -> build/hello.c.o
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)

# Build step for C sources
$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

# Link into program
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CC) $(OBJS) -o $@ 

$(OUTPUT): $(BUILD_DIR)/$(TARGET_EXEC)
	$(TIMING_CMD) $(BUILD_DIR)/$(TARGET_EXEC) > $(OUTPUT) < $(INPUT)

endif

########### C++ compilation #################

CXX := g++
CXXFLAGS := -O3 -Wall -Wextra

# All .c files
CPP_SRCS := $(wildcard *.cpp)

# Conditional CPP build rules
ifneq ("$(CPP_SRCS)","")

# Will compile into .o, for example: hello.c -> build/hello.c.o
OBJS := $(CPP_SRCS:%=$(BUILD_DIR)/%.o)

# Build step for C sources
$(BUILD_DIR)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Link into program
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXXFLAGS) $(OBJS) -o $@ 

$(OUTPUT): $(BUILD_DIR)/$(TARGET_EXEC)
	$(TIMING_CMD) $(BUILD_DIR)/$(TARGET_EXEC) > $(OUTPUT) < $(INPUT)

endif

########### Java compilation #################

JAVA_SRCS := $(wildcard *.java)

# Conditional Java build rules
ifneq ("$(JAVA_SRCS)","")

JAVA_CLASS := $(JAVA_SRCS:%=%.class)

%.class: %.java
	javac $< 

$(OUTPUT): Main.class
	$(TIMING_CMD) java Main > $(OUTPUT) < $(INPUT)

endif

########### Python #################

PYTHON_SRCS := $(wildcard *.py)

# Conditional Java build rules
ifneq ("$(PYTHON_SRCS)","")

$(OUTPUT): main.py
	$(TIMING_CMD) python main.py > $(OUTPUT) < $(INPUT)

endif

######## Comparing Output ##########

# Old comparison
#diff -EBZbq $(OUTPUT) $(EXPECTED_OUTPUT)

.PHONY: check
check: $(OUTPUT)
	./checking.sh $(OUTPUT) $(EXPECTED_OUTPUT)


