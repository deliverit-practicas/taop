
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare_tokens(char* s1, char* s2, const char* delimiter) 
{
	char *pch1, *pch2;
	char *save_ptr1, *save_ptr2;
	pch1 = strtok_r(s1, delimiter, &save_ptr1);
	pch2 = strtok_r(s2, delimiter, &save_ptr2);
	while (pch1 != NULL && pch2 != NULL)
	{
		if ( strcmp(pch1, pch2) != 0 ) {
			printf("Tokens `%s' and `%s' differ\n", pch1, pch2);
			return 1;
		}
		pch1 = strtok_r(NULL, delimiter, &save_ptr1);
		pch2 = strtok_r(NULL, delimiter, &save_ptr2);
	}
	
	if ( pch1 == NULL && pch2 != NULL) {
		printf("Mismatched token: `%s'", pch2);
		return 1;
	}

	if ( pch2 == NULL && pch1 != NULL ) {
		printf("Mismatched token: `%s'", pch1);
		return 1;
	}

	return 0;
}


void show_until_empty_line(FILE* g) 
{
	size_t size = 256;
	char *line = malloc(size);
	ssize_t read = getline(&line, &size, g);
	while ( read != -1 && read != 1 ) {
		printf("%s", line);
		read = getline(&line, &size, g);
	}
}

void show_two_until_empty_line(FILE* g1, FILE* g2) 
{
	size_t size1 = 256;
	char *line1 = malloc(size1);
	size_t size2 = 256;
	char *line2 = malloc(size2);
	ssize_t read1 = getline(&line1, &size1, g1);
	ssize_t read2 = getline(&line2, &size2, g2);
	while ( read1 != -1 && read1 != 1 && read2 != -1 && read2 != 1) {
		line1[read1-1] = 0;
		printf("%s | %s", line1, line2);
		read1 = getline(&line1, &size1, g1);
		read2 = getline(&line2, &size2, g2);
	}
}

void show_case(FILE* input, FILE* output_a, FILE* output_b) 
{
	printf("Outputs mismatch.\n");
	printf("Case input was:\n");
	show_until_empty_line(input);
	printf("\n");
	printf("Expected output | Found output \n");
	show_two_until_empty_line(output_a, output_b);
	printf("\n");
}

int main(int argc, char** argv) 
{
	FILE *input, *output_a, *output_b;
	char *line_i, *line_a, *line_b;
	fpos_t pos_i, pos_a, pos_b;
	size_t size_i = 256, size_a = 256, size_b = 256;
	ssize_t read_a, read_b, read_i;
	const char* delimiter = " \n\t";
	int result = 0;

	if ( argc < 4 ) {
		fprintf(stderr, "Bad number of arguments, 3 arguments expected, %d provided\n", argc-1);
		return 10;
	}

	if ( (input = fopen(argv[1], "r")) == NULL ) {
		fprintf(stderr, "Could not open `%s'\n", argv[1]);
		return 20;
	}

	if ( (output_a = fopen(argv[2], "r")) == NULL ) {
		fprintf(stderr, "Could not open `%s'\n", argv[2]);
		fclose(input);
		return 20;
	}

	if ( (output_b = fopen(argv[3], "r")) == NULL ) {
		fprintf(stderr, "Could not open `%s'\n", argv[2]);
		fclose(input);
		fclose(output_a);
		return 20;
	}

	fgetpos(input, &pos_i);
	fgetpos(output_a, &pos_a);
	fgetpos(output_b, &pos_b);
	
	line_i = malloc(size_i);
	line_a = malloc(size_a);
	line_b = malloc(size_b);
	
	read_a = getline(&line_a, &size_a, output_a);
	read_b = getline(&line_b, &size_b, output_b);
	while ( result == 0 && read_a != -1 && read_b != -1 ) {
		/* Lines are different */
		if ( compare_tokens(line_a, line_b, delimiter) ) {
			// Rewind to begining of case
			fsetpos(input, &pos_i);
			fsetpos(output_a, &pos_a);
			fsetpos(output_b, &pos_b);
			// Show whole case
			show_case(input, output_a, output_b);
			result = 1; /* end loop if different */
		} else if ( read_a == 1 ) { /* Empty line */ 
			/* move forward the actual case */
			fgetpos(output_a, &pos_a);
			fgetpos(output_b, &pos_b);
			read_i = getline(&line_i, &size_i, input);
			while ( read_i != -1 && read_i != 1 ) {
				read_i = getline(&line_i, &size_i, input);
			}
			fgetpos(input, &pos_i);
		}
		read_a = getline(&line_a, &size_a, output_a);
		read_b = getline(&line_b, &size_b, output_b);
	}
	
	if ( result == 0 && read_b != -1 ) {
		printf("Expected output ends but found output does not. Do you write an extra line?\n");
		result = 1;
	}

	if ( result == 0 && read_a != -1 ) {
		printf("Found output ends unexpectedly. You probably miss some test cases\n");
		result = 1;
	}

	free(line_i);
	free(line_a);
	free(line_b);
	fclose(input);
	fclose(output_a);
	fclose(output_b);

	return result;
}
