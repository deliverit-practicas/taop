# sudo docker build -t "con_apk" https://stapia@bitbucket.org/deliverit-practicas/taop.git#:folder

FROM python:slim

# Install Java 
RUN apt-get update && apt-get install -y --no-install-recommends \
		default-jdk \
		build-essential \
	&& apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false 

CMD /bin/bash
